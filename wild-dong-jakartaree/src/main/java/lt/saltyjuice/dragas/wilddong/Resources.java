package lt.saltyjuice.dragas.wilddong;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.reflect.Member;
import java.util.logging.Logger;

@Named
@ApplicationScoped
public class Resources implements Serializable {

    @Produces
    public Logger getLogger(InjectionPoint ip) {
        Member member = ip.getMember();
        return Logger.getLogger(member.getDeclaringClass().getName());
    }

    @Produces
    @MaxNumber
    @ApplicationScoped
    public int getDefaultNumber() {
        return 100334;
    }
}
