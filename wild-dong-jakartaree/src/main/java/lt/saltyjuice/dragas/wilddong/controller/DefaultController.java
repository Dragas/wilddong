package lt.saltyjuice.dragas.wilddong.controller;

import lt.saltyjuice.dragas.wilddong.MaxNumber;
import lt.saltyjuice.dragas.wilddong.entity.InputEntity;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.TransientReference;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.SecurityContext;
import java.io.Serializable;
import java.util.logging.Logger;

@Named("currentController")
@SessionScoped
public class DefaultController implements Serializable {

    @Inject
    private FacesContext facesContext;

    @Inject
    private HttpSession session;

    private String username;
    private String someOutput;

    @Inject
    @MaxNumber
    private int number;

    private int count = 10;

    public void create() {
        FacesMessage message = new FacesMessage();
        message.setDetail(String.format("Entered username %s", username));
        message.setSeverity(FacesMessage.SEVERITY_INFO);
        message.setSummary("Entered username");
        facesContext.addMessage(session.getId(), message);
        facesContext.addMessage(session.getId(), message);
        facesContext.addMessage(session.getId(), message);
        facesContext.addMessage(session.getId(), message);
        //logger.info(String.format("[%1$s] entered username %2$s", session.getId(), username));
        someOutput = String.format("Entered %s for input", username);
        username = null;
    }

    public int getCount() {
        return count;
    }

    public void decrement() {
        count--;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSomeOutput() {
        return someOutput;
    }
}
