package lt.saltyjuice.dragas.wilddong.entity;

public class InputEntity {
    private String input;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
