package lt.saltyjuice.dragas.wilddong;

import javax.inject.Qualifier;
import java.lang.annotation.*;

@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Qualifier
public @interface MaxNumber {

}
